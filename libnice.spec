Name:           libnice
Version:        0.1.22
Release:        1
Summary:        An implementation of ICE standard
License:        LGPLv2 and MPLv1.1
URL:            https://libnice.freedesktop.org/
Source0:        https://nice.freedesktop.org/releases/%{name}-%{version}.tar.gz

Patch0:         libnice-gupnp-1.6.patch

BuildRequires:  autoconf automake glib2-devel gnutls-devel >= 2.12.0
BuildRequires:  gobject-introspection-devel gstreamer1-devel >= 0.11.91
BuildRequires:  gstreamer1-plugins-base-devel >= 0.11.91
BuildRequires:  gtk-doc gupnp-igd-devel >= 0.1.2 graphviz meson
BuildRequires:  cmake gnupg2

%description
Libnice is an implementation of the IETF's Interactive Connectivity
Establishment (ICE) standard (RFC 5245). It provides a GLib-based
library, libnice, as well as GStreamer elements.
ICE is useful for applications that want to establish peer-to-peer UDP
data streams. It automates the process of traversing NATs and provides
security against some attacks. It also allows applications to create
reliable streams using a TCP over UDP layer.

%package        gstreamer1
Summary:        GStreamer plugin for libnice
Requires:       %{name} = %{version}-%{release}

%description    gstreamer1
This package provides a gstreamer 1.0 plugin for libnice.

%package        devel
Summary:        Development files for libnice
Requires:       %{name} = %{version}-%{release} glib2-devel pkgconfig

%description    devel
This package provides Libraries and header files for libnice.

%prep
%autosetup -n %{name}-%{version} -p1
sed -e "s/^  'test-set-port-range'/#&/" -i tests/meson.build

%build
%meson -D gtk_doc=enabled
%meson_build

%install
%meson_install
%delete_la

%check
%meson_test

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files
%doc NEWS README
%license COPYING COPYING.LGPL COPYING.MPL
%{_bindir}/{stunbdc,stund}
%{_libdir}/{*.so.*,girepository-1.0/Nice-0.1.typelib}

%files gstreamer1
%{_libdir}/gstreamer-1.0/libgstnice.so

%files devel
%{_includedir}/*
%{_libdir}/{*.so,pkgconfig/nice.pc}
%{_datadir}/{gtk-doc/html/libnice/,gir-1.0/Nice-0.1.gir}

%changelog
* Wed Nov 13 2024 xu_ping <707078654@qq.com> - 0.1.22-1
- Update to 0.1.22
- Make nice_address_is_local() available to applications.
- Fix interface listing on Android
- Make padding be all zeros to conform to RFC8489
- Fix ifr_ifindex build with cland and OpenBSD

* Wed Oct 11 2023 Ge Wang <wang__ge@126.com> - 0.1.21-1
- Update to 0.1.21

* Mon Aug 07 2023 xuping <707078654@qq.com> - 0.1.19-2
- fix build unresolvable due to gupnp upgrade

* Thu Aug 04 2022 yaoxin <yaoxin30@h-partners.com> - 0.1.19-1
- Update to 0.1.19

* Tue Mar 29 2022 wangkai <wangkai385@h-partners.com> - 0.1.14-12
- Do check on X86 and ARM64

* Tue Mar 29 2022 yaoxin <yaoxin30@huawei.com> - 0.1.14-11
- Repair failed to execute "gtkdocize"

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 0.1.14-10
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Fri Oct 25 2019 yanzhihua <yanzhihua4@huawei.com> - 0.1.14-9
- Package init
